package com.spf.goodsService.model;

import lombok.Data;

import java.util.List;

@Data
public class Goods {
    String id;
    String name;
    String price;
    List<String> imageUuids;
}
