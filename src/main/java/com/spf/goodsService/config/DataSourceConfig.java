package com.spf.goodsService.config;

import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.context.annotation.Bean;

public class DataSourceConfig {
    @Bean
    public PGPoolingDataSource getDataSource() {
        PGPoolingDataSource dataSource = new PGPoolingDataSource();

        dataSource.setUrl("/localhost:5432");
        dataSource.setUser("pgUser");
        dataSource.setPassword("1488");

        return dataSource;
    }
}
