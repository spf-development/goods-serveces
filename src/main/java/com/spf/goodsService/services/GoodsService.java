package com.spf.goodsService.services;

import com.spf.goodsService.model.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsService {

    @Autowired
    DataSource dataSource;

    public List<Goods> searchGoods(String searchText) {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        List<Goods> goodsList = new ArrayList<>();

        try {

            connection = dataSource.getConnection();
            statement = connection.prepareStatement("");
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Goods good = new Goods();
                good.setId(resultSet.getString(1));
                good.setName(resultSet.getString(2));
                good.setPrice(resultSet.getString(3));
                //good.setImageUuids(resultSet.getArray());

                goodsList.add(good);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
                statement.close();
                resultSet.close();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
        return goodsList;
    }
}
