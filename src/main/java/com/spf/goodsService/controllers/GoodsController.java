package com.spf.goodsService.controllers;

import com.spf.goodsService.model.Goods;
import com.spf.goodsService.services.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class GoodsController {

    @Autowired
    GoodsService goodsService;

    @GetMapping("/search")
    public List<Goods> searchGoods(@RequestParam("SearchText") String searchText){
        return goodsService.searchGoods(searchText);
    }
}
